Please welcome /e/OS 2.0! :rocket:

We are proud to deliver the /e/OS 2.0. Enjoy all the new features and improvements it embeds!

## ✨ We embedded some improvements! 




:sunrise: **Launcher**
- The launcher has been upgraded which solves a lot of bugs and also brings improvements like displaying the number of notifications and being able to set live wallpapers
- Brand new icons and wallpapers :sparkles: 

🦸 **Third Party Apps support**
- /e/OS now supports Android Auto :tada: Please refer to [the documentation](https://doc.e.foundation/support-topics/android-auto) to know more.
- Wording of the application licensing notification has been made clearer

:camera: **Camera**
- QR code scanning is now available in Camera from the lower left side button :tada: 

:shield: **Advanced Privacy**
- A Wall of Shame has been added to the homepage the identify the most leaking apps and trackers
- UI improvements

:pencil: **Notes**
- It's now possible to use the Notes apps without any account

:shopping_bags: **App Lounge**
- Search can now filter apps with no trackers or Open Source apps or Progressive Web apps
- It's now possible to ignore the refresh session message

🔄 **File Synchronization**
- Updated to version 1.5 of eDrive for more stability of the feature


:globe_with_meridians: **Browser**
- Sensor exposure is reenabled
- Murena.com and Murena.io have been added as shortcuts 

:date: **Calendar** 
- Past events are now visible up to one year in the past instead of 3 months


:iphone: **Fairphone 5**

- It's now possible to select a preferred network type from a simplified list


:iphone: **SHIFT 6mq**

- IMG package is now supported


:telephone_receiver: **Dialer**
- VoLTE icon from the status bar was removed as it was confusing for many users.


:book: **PDF reader**
- App now has its own adaptive icon


:gear: **Settings**
- Long pressing the power button to get the flashlight has been made the default setting
- The additional Volume options available when playing music over Bluetooth are now following our guidelines




## 🕙 Software updates
 
➕ **LineageOS 20** latest bug fixes and security updates have been merged ([list](https://review.lineageos.org/q/branch:lineage-20.0+status:merged+after:%222024-02-27+19:00:00+%252B0100%22+before:%222024-04-24+09:00:00+%252B0100%22))


:globe_with_meridians: **Browser** updated to version 123.0.6312.122 from upstream 

:speech_balloon: **Message** updated to v3.10 from upstream

TT4C and TT46 FPOS updates are included for :iphone: **Fairphone 5**

🦸 microG updated to v0.3.1.240913 from upstream


## 🐛 Bug fixes


:shopping_bags: **App Lounge**
- Google sign-in page is available again and Google Workspace accounts can connect through it


:date: **Calendar** 
- reminder notification no longer pops-up after event has ended


:iphone: **Fairphone 5**

- Call quality using speakerphone has been improved
- GPS coordinates are now the expected ones in Camera


:park: **Gallery**
- When setting Gallery's widget in light mode, offered options are now visible 
- Pictures' coordinates are now displayed properly when saved

:book: **PDF reader**
 - App no longer crashes with certain type of PDF documents


:gear: **Settings**
- It's now possible to successfully program a NFC tag with the Profile Manager 



## ⚠ Security fixes

This /e/OS v2.0 version includes the [Android security patches](https://source.android.com/docs/security/bulletin/2024-04-01) available until April 2024. 
