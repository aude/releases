# Please welcome /e/OS 1.17! :rocket:

We are proud to deliver the /e/OS 1.17. Enjoy all the new features and improvements it embeds!

## ✨ We embedded some improvements!

Keyboard
-  Keyboard's redesign in Android 13 to match our palette

Community devices
-  Some community devices were missing images for flashing: fix and update documentation   


Browser

Updater
-  Updater crashes prevention    
-  Translation improvements

microG 
-  We moved to Stadia map tiler backend

App Lounge 
-  The color of the banner now adapts to the theme in use    


## 🕙 Software updates

Browser update
- Browser update including security patches

microG
- microG update with upstream    

➕ **LineageOS 20.0** last bug fixes and security updates has been merged ([list](https://review.lineageos.org/q/branch:lineage-20.0+status:merged+after:%222023-09-20+21:33:00+%252B0200%22+before:%222023-10-23+20:50:00+%252B0200%22))

FP5
-  Upgrade firmware to FP5.TT3P.A.112.20231016   


## 🐛 Bug fixes

Browser
-  Bookmark folders in Browser are now functional
-  Inapp browser integration is now working instead of opening a full browser instance

Message
-  SMS messages are now sent or received SMS messages only once as expected 

Settings
-  Subsettings in Settings for A13 do not crash anymore
- The notification light option is not available in Settings when there is no notification light on the phone

Update
- Long text strings are now perfectly readable in the Updater
-  OTA server now only lists upgrades which are suitable for the phone


App Lounge
-  PWA label is again displayed in search results
-  AppLounge opens F-Droid links properly
-  Search results are now exhaustive whatever the source
-  Silent notifications for App Lounge now follow our palette     
-  App lounge now warns user when space is insufficient to handle updates    

eDrive
-  eDrive does not get stuck while syncing anymore

FP5
-  Option to select Calling/Roaming preference under Use Wi-Fi calling is now available with Congstar (T-Mobile Germany)
- The "Serious camera error" is now fixed for FairPhone5

FP4
- Camera lens are now all available in Camera for Fairphone 4
-  Pixel 4a, Pixel 6a and Fairphone 4 on Android 13 do not reboot randomly anymore

Pixel 5
-  Make boot and dtbo images available    


Community devices
-  Xiaomi variants are nnow built for A12
-  [other] axolotl: fork trees from Lineage https://gitlab.e.foundation/e/backlog/-/issues/7080 @Lacentix       
 
## ⚠ Security fixes

This /e/OS v1.17 includes the [Android security patches](https://source.android.com/security/bulletin/2023-10-01) available until October.
